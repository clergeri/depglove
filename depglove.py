#!/usr/bin/env python
# implementation of a variant of Glove algorithm on dependencies
# the main difference is the use of matrices M_r for relations r and a single set of vectors for words
# the objective function is J = \Sigma_{i,r,j} f(X_{i,r,j}) (w_i^T.M_r.w_j+b_i+b_j+b_r-log(X_{irj}))

import os
import sys
import re
import collections
from math import log, sqrt
from argparse import ArgumentParser
import fileinput

import itertools
from functools import partial
import logging

from random import shuffle
import numpy as np
from scipy import sparse

import cPickle as pickle
from functools import partial

logger = logging.getLogger("depglove")

keep = { 'modifieur': 1,
         'sujet': 1,
         'de':1,
         'cod':1,
         'et':1,
         'complement':1,
         'de=':1,
         'ou':1,
         ',':1,
         'de*':1,
         'antemodifieur':1,
        'pour':1
    }

def parse_args():
    parser = ArgumentParser(
        description=('build a GloVe vector-space model from a list of collected and counted dependency tripples')
    )

    g_cooccur = parser.add_argument_group('Cooccurrence tracking options')
    g_cooccur.add_argument('--min-wcount', type=int, default=10,
                           help=('Discard cooccurrence pairs where at '
                                 'least one of the words occurs fewer '
                                 'than this many times in the training '
                                 'corpus'))
    g_cooccur.add_argument('--min-rcount', type=int, default=10,
                           help=('Discard cooccurrence pairs where '
                                 'the relation occurs fewer '
                                 'than this many times in the training '
                                 'corpus'))
    
    g_glove = parser.add_argument_group('GloVe options')
    g_glove.add_argument('--vector-path', default='mdl',
                         help=('Path to which to save computed word '
                               'vectors'))
    g_glove.add_argument('-s', '--vector-size', type=int, default=100,
                         help=('Dimensionality of output word vectors'))
    g_glove.add_argument('--iterations', type=int, default=25,
                         help='Number of training iterations')
    g_glove.add_argument('--learning-rate', type=float, default=0.05,
                         help='Initial learning rate')
    g_glove.add_argument('--save-often', action='store_true', default=False,
                         help=('Save vectors after every training '
                               'iteration'))
    g_glove.add_argument('--toefl',default=None,
                         help='check against toefl tests at each iteration'
                         )
    return parser.parse_args()

def rsel_filter():
    for line in sys.stdin:
        v = line.strip().split("\t")
    #    print "Triple %s" % v[:5]
        g,r,d,n,w = v[:5]
        w = float(w)
        # remove hapax
#        if w == 1: continue
        # remove very general dependencies
        if (r == ',' or r == 'ou'):
            r = 'et'
        if (d == 'cln_cln'  or '_Uv' in d or '_Uv' in g) : continue
        if r == 'cod': r = 'objet'
        if (r == 'objet' and ('_v' in d or '_aux' in d)):
            d = '*sentence*_v'
        if (r == 'attribut') :
            r = 'modifieur'
        if (('+' in r or '/' in r) and all(x not in r for x in ('sujet','cod','modifieur','antemodifieur','subject','object','xcomp'))):
            r = 'undef'
            w /= 2.0
        yield (g,r,d,w)

        mg = re.search(':(_[A-Z]+\S*_np)',g)
        if mg != None: yield (mg.group(1),r,d,w/2.0)

        md = re.search(':(_[A-Z]+\S*_np)',d)
        if md != None: yield (g,r,md.group(1),w/2.0)

        if mg != None and md != None: yield (mg.group(1),r,md.group(1),w/4.0)

        mr = re.match('^(.+)\+.+\+(.+)$',r)
        if mr is not None: yield (g,mr.group(1)+'+'+mr.group(2),d,w/2.0)
                            
def rsel_load(min_wcount=None,min_rcount=None):
    rels={}
    vocab={}
    for (g,r,d,w) in rsel_filter():
        if g not in vocab: vocab[g] = 0.0
        vocab[g] += w
        if d not in vocab: vocab[d] = 0.0
        vocab[d] += w

        if r not in rels:
            rels[r] = {}
        if (g,d) not in rels[r]:
            rels[r][g,d] = 0.0
        rels[r][g,d] += w

    vocab2 = [(w,vocab[w]) for w in vocab if vocab[w] > min_wcount]
    word2id = dict((w,i) for i,(w,_) in enumerate(vocab2) )
    
    rels2 = dict(( r, [(word2id[g],word2id[d],rels[r][g,d]) for (g,d) in rels[r] if g in word2id and d in word2id])
                for r in rels
#                if len(rels[r]) > min_rcount
                )
        
    logger.info("vocab size %i relation size: %i",len(vocab2),len(rels2))

#    for r in rels2:
#        print "%s len=%i %s" % (r,len(rels2[r]),rels2[r])

    return (vocab2,rels2,word2id)

def run_iter(vocab,data, learning_rate=0.05):
    global_cost = 0
    shuffle(data)
    for (v_main, v_context, m_r,
         b_main, b_context, b_r,
         gradsq_W_main, gradsq_W_context, gradsq_M_r,
         gradsq_b_main, gradsq_b_context, gradsq_b_r,
         weight,
         log_cooccurrence) in data:
        cost_inner = (v_main.dot(m_r).dot(v_context)
                      #v_main.dot(v_context)
                      + b_main[0] + b_context[0]
                      + b_r[0]
                      - log_cooccurrence)
        cost = weight * cost_inner
        global_cost += 0.5 * cost * cost_inner
                
        # Computed gradients for vectors and matrices
        grad_main = cost * m_r.dot(v_context)
        grad_context = cost * v_main.dot(m_r)
        grad_r = cost * np.outer(v_main,v_context)
        
        # Compute gradients for bias terms
        # grad_bias_main = cost
        # grad_bias_context = cost
        # grad_bias_r = cost
        
        # Now perform adaptive updates
        v_main -= (learning_rate * grad_main / np.sqrt(gradsq_W_main))
        v_context -= (learning_rate * grad_context / np.sqrt(gradsq_W_context))
        m_r -= (learning_rate * grad_r / np.sqrt(gradsq_M_r))

        lcost = learning_rate * cost
        
        b_main -= (lcost / np.sqrt(gradsq_b_main))
        b_context -= (lcost / np.sqrt(gradsq_b_context))
        b_r -= (lcost / np.sqrt(gradsq_b_r))
        
        # Update squared gradient sums
        gradsq_W_main += np.square(grad_main)
        gradsq_W_context += np.square(grad_context)
        gradsq_M_r += np.square(grad_r)

        cost **= 2
        
        gradsq_b_main += cost
        gradsq_b_context += cost
        gradsq_b_r += cost
            
    return global_cost

def glove_train(vocab,rels,
                iter_callback=None, 
                vector_size=100,
                iterations=25,
                x_max=100,
                alpha=0.75,
                toefl=None,
                **kwargs):
    """ Train glove vectors using vocabulary and relations. Coocurrence info are stored inside rels
    """

    sumr = dict((r,sum([occ for _,_,occ in rels[r]])) for r in rels)
    for r in sumr:
        if sumr[r] > 0.0:
            continue
        print "null r %s: %s" % (r,rels[r])

    alln = sum(weight for (_,weight) in vocab)
    allr = sum(sumr[r] for r in sumr)
    coeff = log(alln / allr)

    vsize = len(vocab)
    W = (np.random.rand(vsize, vector_size) - 0.5) / float(vector_size + 1)
    biases = (np.random.rand(vsize) - 0.5) / float(vector_size + 1)
    # biases = np.array([0.5*log(weight) for (_,weight) in vocab])
    
    gradient_squared = np.ones((vsize, vector_size),dtype=np.float64)
    gradient_squared_biases = np.ones(vsize, dtype=np.float64)

    Wr = dict((r,
               (#np.identity(vector_size,dtype=np.float64),
                np.random.rand(vector_size, vector_size) / float(vector_size + 1),
                np.ones((vector_size,vector_size),dtype=np.float64),
                #np.array([0.1 * (log(1+sumr[r])+coeff)]),
                (np.random.rand(1) - 0.5) / float(vector_size + 1),
                np.ones(1,dtype=np.float64)
               )) for r in rels)
    
    data = [(W[gid],W[did],Wr[r][0],
             biases[gid:gid+1],biases[did:did+1],Wr[r][2],
             gradient_squared[gid], gradient_squared[did], Wr[r][1],
             gradient_squared_biases[gid:gid+1], gradient_squared_biases[did:did+1], Wr[r][3],
             (float(cooccurrence) / x_max) ** alpha if cooccurrence < x_max else 1.0,
             log(float(cooccurrence))
             )
            for r in rels
            for (gid,did,cooccurrence) in rels[r]]
        
    for i in range(iterations):
        logger.info("\tBeginning iteration %i..", i)
        cost = run_iter(vocab, data, **kwargs)
        logger.info("\t\tcost=%i",cost)
        if toefl is not None:
            toefl_test(toefl,W,i)
        if iter_callback is not None:
            iter_callback(vocab,W,Wr)
        
    return (W,Wr)

def save_model(vocab,W, Wr, path):
    with open(path, 'wb') as vector_f:
        pickle.dump(([w for (w,_) in vocab],W), vector_f, protocol=2)

    logger.info("Saved vectors to %s", path)

def toefl_load(toeflfile,word2id):
    test = {}
    alltest = 0
    ktest = 0
    for line in open(toeflfile,"r"):
        m=re.match('^test\s+id=(\d+)\s+"(.+?)"\s+"(.+?)"\s+"(.+?)"\s+"(.+?)"',line)
        if m != None:
            tid = m.group(1)
            alltest += 1
            ws = [m.group(2),m.group(3),m.group(4),m.group(5)]
            if all(w in word2id for w in ws):
                test[tid] = [word2id[w] for w in ws]
                ktest += 1
    logger.info("toefl %s: kept %d tests out of %d (r=%.2f%%)",toeflfile,ktest,alltest,ktest * 100.0 / alltest)
    return test

def toefl_test(test,W,iter):
    success = 0
    s = {}
    ktest = len(test)
    for t in test:
        ids = test[t]
        ds = [np.dot(W[ids[0]]/np.linalg.norm(W[ids[0]]),W[id]/np.linalg.norm(W[id])) for id in ids[1:4]]
        if ds[0] > ds[1] and ds[0] > ds[2]:
            success += 1
    logger.info("\titer %d toefl success %d/%d (r=%.2f%%)",iter,success,ktest, 100.0 * success / ktest)

def main(arguments):
    if arguments.vector_path:
            fh = logging.FileHandler(arguments.vector_path + '.log')
            logger.addHandler(fh)    
    logger.info("Loading rsel triples..")
    vocab,rels,word2id = rsel_load(min_wcount=arguments.min_wcount,
                            min_rcount=arguments.min_rcount
                )
    if arguments.save_often:
        iter_callback = partial(save_model, path=arguments.vector_path)
    else:
        iter_callback = None

    toefl = None    
    if arguments.toefl != None:
        toefl = toefl_load(arguments.toefl,word2id)
        
    logger.info("Glove training..")
    (W,Wr) = glove_train(vocab,rels,
                        iter_callback=iter_callback,
                        vector_size=arguments.vector_size,
                        iterations=arguments.iterations,
                        learning_rate=arguments.learning_rate,
                        toefl = toefl
                        )
     
    if not arguments.save_often:
        save_model(vocab,W,Wr, arguments.vector_path)

if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format="%(asctime)s\t%(message)s")
    main(parse_args())

