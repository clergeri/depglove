#!/usr/bin/env python
# evaluation of depglove vectors (see depglove.py)

from argparse import ArgumentParser
from functools import partial
import cPickle as pickle
import readline # optional, will allow Up/Down/History in the console
import code
from sklearn.cluster import MiniBatchKMeans

import numpy as np
import re

import readline
import rlcompleter
import os
import atexit

from scipy.cluster.hierarchy import to_tree
from fastcluster import linkage
from matplotlib import pyplot as plt

historyPath = os.path.expanduser("~/.depglove_history")

def save_history(historyPath=historyPath):
    import readline
    readline.write_history_file(historyPath)

if os.path.exists(historyPath):
    readline.read_history_file(historyPath)

readline.parse_and_bind("tab: complete")

atexit.register(save_history)

def make_id2word(vocab):
    return dict((id, word) for word, (id, _) in vocab.iteritems())


def most_similar(W, vocab, id2word, words, n=15):
    """
    Find the `n` words most similar to the given list of 'words'. The provided
    `W` must have unit vector rows, and must have merged main- and
    context-word vectors (i.e., `len(W) == len(word2id)`).
    Returns a list of word strings.
    """

    assert len(W) == len(vocab)

    for word in words:
        if word not in vocab:
            print("*** no vector for word %s" % word)
            return []

    wids = [vocab[word] for word in words]    
    v = np.mean([W[wid] for wid in wids],axis=0)
    v /= np.linalg.norm(v)
    
    dists = np.dot(W, v)
    top_ids = np.argsort(dists)[::-1][:n + 1]

    return [(id2word[id],dists[id]) for id in top_ids if id not in wids][:n]

def analogy(W,vocab,id2word,a,b,c,n=15):
    """
    Find the 'n' words 'd' most susceptible to complete the analogy d is to 'c' what 'b' is to 'a'
    Returns a list of word strings.
    """
    assert len(W) == len(vocab)

    if a not in vocab:
        print("*** no vector for word %s" % a)
        return []
    if b not in vocab:
        print("*** no vector for word %s" % b)
        return []
    if c not in vocab:
        print("*** no vector for word %s" % c)
        return []

    aid = vocab[a]
    bid = vocab[b]
    cid = vocab[c]
    dv = W[bid]+W[cid]-W[aid]
    dv /= np.linalg.norm(dv) 
    dists = np.dot(W,dv)
    top_ids = np.argsort(dists)[::-1][:n+3]

    return [(id2word[id],dists[id]) for id in top_ids if id != aid and id != bid and id != cid][:n]

def cluster(W,vocab,file):
    """
    Clusterize Glove vectors
    """
    vsize = len(vocab)
    reduction_factor = 0.1
    clusters_to_make  = int( vsize * reduction_factor ) # The number of clusters to make
    kmeans_model      = MiniBatchKMeans(init='k-means++', n_clusters=clusters_to_make, n_init=10)
    kmeans_model.fit(W)
    cluster_labels    = kmeans_model.labels_
    cluster_inertia   = kmeans_model.inertia_
    cluster_to_words = {}
    for c,i in enumerate(cluster_labels):
        if i not in cluster_to_words:
            cluster_to_words[i] = []
        cluster_to_words[i].append(vocab[c])
    with open(file,"w") as f:
        for c in cluster_to_words:
            f.write("<%i> %s\n" % (c,' '.join([w.replace(' ','_') for w in cluster_to_words[c]])))
        f.close()

def cluster_dendro(W,vocab,file):
    """
    Clusterize Glove vectors using AgglomerativeClustering
    """
    # vsize = len(vocab)
    W2 = np.array([v for (v,w) in zip(W,vocab) if '_np' not in w])
    vocab2 = [w for (v,w) in zip(W,vocab) if '_np' not in w]
    Z = linkage(W2,'ward')
    # d = dendrogram(Z)
    # plt.show()
    with open(file,"w") as f:
        T = to_tree(Z)
        stack = [(T,'')]
        while stack:
            (r,p) = stack.pop()
            if r.is_leaf():
                id = r.get_id()
                w = vocab2[id]
                f.write("%s\t%s\n" % (w,p))
            else:
                stack.append((r.get_left(),p+'0'))
                stack.append((r.get_right(),p+'1'))
        f.close()
            
def wordnet(wnfile,W,vocab,id2word):
    test = {}
    alltest = 0
    ktest = 0
    for line in open(wnfile,"r"):
        m=re.match('^test\s+id=(\d+)\s+"(.+?)"\s+"(.+?)"\s+"(.+?)"\s+"(.+?)"',line)
        if m != None:
            tid = m.group(1)
            alltest += 1
            ws = [m.group(2),m.group(3),m.group(4),m.group(5)]
            if all(w in vocab for w in ws):
                test[tid] = ws
                ktest += 1
#            else:
#                print ("rejected %s" % ws)
    print "kept %d tests out of %d (r=%.2f%%)" % (ktest,alltest,ktest * 100.0 / alltest)
    success = 0
    s = {}
    for t in test:
        ws = test[t]
        ids = [vocab[w] for w in ws]
        ds = [np.dot(W[ids[0]],W[id]) for id in ids[1:4]]
        end = ws[0].split('_')[-1]
#        end = end[-1]
        if end not in s:
            s[end] = [0,0]
        s[end][0] += 1
        if ds[0] > ds[1] and ds[0] > ds[2]:
            success += 1
            s[end][1] += 1
            
    print "\tsuccess %d (r=%.2f%%)" % (success,100.0 * success / ktest)
    for end in s:
        print "\t\t%s: success %d (r=%.2f%%)" % (end,s[end][1],100.0 * s[end][1] / s[end][0])
         
def save_word2vec(W,vocab,file):
    """
    save vectors following word2vec vector format
    """
    with open(file,"w") as f:
        f.write("%i\n" % len(W))
        f.write("%i\n" % len(W[0]))
        for w,v in zip(vocab,W):
            f.write("%s %s\n" % (w.replace(' ','_'),' '.join(["%s" % u for u in v])))
        f.close()

def parse_args():
    parser = ArgumentParser(
        description=('Evaluate a GloVe vector-space model on a word '
                     'analogy test set'))

    parser.add_argument('--mdl', type=partial(open, mode='rb'),
                        help=('Path to serialized vectors file as '
                              'produced by this GloVe implementation'))

    
    return parser.parse_args()

def main(arguments):
    (vocab,W) = pickle.load(arguments.mdl)
    #print "vocab: %s" % vocab
    id2word = {i: w for i,w in enumerate(vocab)}
    word2id = {w: i for i,w in enumerate(vocab)}
    vsize = len(vocab)
    if vsize < len(W):
        print "double set of word vectors"
    for i,row in enumerate(W[:vsize]):
        merged = (row+W[i+vsize])/2 if len(W) > vsize else row
        merged /= np.linalg.norm(merged) 
        W[i] = merged
    W=W[:vsize]
    while True:
        try:
            w = raw_input('Enter a word or quit: ')
        except:
            break
        w.strip()
        if w == 'quit':
            break
        if w == 'list':
            print("vocab: %s" % vocab)
            continue
        m=re.match('word2vec\s+(\S+)',w)
        if m != None:
            save_word2vec(W,vocab,m.group(1))
            continue
        m=re.match('cluster\s+(\S+)',w)
        if m != None:
            cluster_dendro(W,vocab,m.group(1))
            continue
        m=re.match('wordnet\s+(\S+)',w)
        if m != None:
            wnfile = m.group(1)
            wordnet(wnfile,W,word2id,id2word)
            continue
        m=re.match('(\S+)\s+:\s+(\S+)\s+::\s+(\S+)',w)
        if m != None:
            a = m.group(1)
            b = m.group(2)
            c = m.group(3)
            # analogy: look for d such that d is to c what b is to a
            # e.g.: look for d such that d is to roi what femme is to homme
            for (i,(d,score)) in enumerate(analogy(W,word2id,id2word,a,b,c)):
                print("\t%i: %s (%s)" % (i,d,score))
            continue
        ws=re.split('\s*,\s*',w)
        for i,(w2,score) in enumerate(most_similar(W,word2id,id2word,ws)):
            print("\t%i: %s (%s)" % (i,w2,score))
            
    print("Goodbye !")

if __name__ == '__main__':
    main(parse_args())
