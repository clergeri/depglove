#!/usr/bin/env perl

## a small server for playing with word vector (built with depglove)

use strict;
use warnings;
use Mojolicious::Lite;
use Mojo::DOM;
use Mojo::ByteStream 'b';
use Mojo::Log;
use PDL::Lite;
use PDL::Primitive;
use Math::Big;
use List::AllUtils qw(reduce);

=head1 NAME

depglove_server.pl -- Experimental server for word vectors based on Mojolicious

=head1 SYNOPSIS

   morbo ./depglove_server.pl [options]
   hypnotoad ./depglove_server.pl [options]
   plackup ./depglove_server.pl -s FCGI --port 3000
   plackup ./depglove_server.pl -s FCGI --port 3000 -D

=head1 DESCRIPTION

B<depglove_server.pl> is an experimental Perl server to ease the
use of word vectors built with depglove. It runs on all WebServers provided by Mojolicious.

=head1 OPTIONS

=head1 SEE ALSO

=over 4

=item Alpage project team L<http://alpage.inria.fr>

=item Mojolicous::Lite

=back

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013, 2014, 2015, 2016, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=cut

######################################################################
## frmg_shell initialization

use AppConfig qw/:argcount :expand/;

my $depglove_config = 
  app->{depglove}{config} = 
  AppConfig->new(
		 "verbose!" => {DEFAULT => 0},
		);

######################################################################
## Server part

app->secrets('il observe une maman avec ses jumelles');
app->mode('production');

my $server_config = plugin 'Config' => { file => 'depglove_server.conf' };

plugin('MoreUtilHelpers', maxwords => { max => 100 });

plugin AssetPack => {
		     pipes => [qw{Css JavaScript Combine}]
		    };

app->asset->process(
		    'app.css' => (
				  "http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"
				 )
		   );

app->asset->process(
		    'app.js' => (
				 "http://code.jquery.com/jquery-1.10.2.js",
				 "http://code.jquery.com/ui/1.11.4/jquery-ui.js",
				)
		   );

my $ws = Mojo::Transaction::WebSocket->new;

# Customize log file location and minimum log level
my $log = Mojo::Log->new(path => "$server_config->{cfgdir}/server.log", level => 'info');

my $vectors = vector_import($server_config->{vectors});

sub vector_import {
  my $vector_file = shift;
  -f $vector_file or die;
  open(V,'<',$vector_file)
    || die "can't process $vector_file:$!";
  my $n = <V>; chomp $n;
  my $dim = <V>; chomp $dim;
  while(<V>) {
    chomp;
    my ($wcat,@v) = split(/\s+/,$_);
    my ($w,$cat) = $wcat =~ /^(.+)__(\w+?)$/;
    $vectors->{$wcat} = norm pdl [@v];
  }
  close(V);
  print "processed $vector_file\n";
  return $vectors;
}

sub find_closest_words {
  my @words = @_;
#  print "Process words @words\n";
  @words = grep {exists $vectors->{$_->[1]}}
    map {s/^-//o ? [-1,$_] : [1,$_]} @words;
  @words or return [];
  my $avg = norm reduce {$a+$b} map {$_->[0] * $vectors->{$_->[1]}} @words;
#  print "Process2 words @words\n";
  my %w = ();
  $w{$_->[1]} = 1 foreach (@words);
  my @u = ();
  my $min = -1000;
  foreach my $x (
		 map {[$_,inner($avg,$vectors->{$_})]}
		 grep {!exists $w{$_}}
		 keys %$vectors
		) {
    if (@u < 10) {
      @u = sort {$b->[1] <=> $a->[1]} $x,@u;
      $min = $u[-1][1];
    } elsif ($x->[1] > $min) {
      pop @u;
      @u = sort {$b->[1] <=> $a->[1]} $x,@u;
      $min = $u[-1][1];
    }
  }
#  print "found @u\n";
  return \@u;
}

#plugin 'Log::Access';
##plugin 'Log::Timestamp' => {pattern => '%y%m%d %X'};

helper ip => sub {
  my $c = shift;
  return $c->req->headers->header('X-Real-IP')
    || $c->tx->remote_address;
};

my $ip;

$log->format(sub {
	       my ($time, $level, @lines) = @_;
	       return '[' . localtime(time) . "] [$level] [$ip] " . join("\n", @lines) . "\n";
	     });

helper depglove => sub { 
  my $self = shift;
  my $param = shift;
  return app->{depglove}{config}->get($param);
};

helper img => sub {
  my $self = shift;
  my $data = shift;
  return b($data)->b64_encode('');
};

helper graphical => sub {
  my $self = shift;
  my $format = app->{depglove}{config}->format;
  return grep {$format eq $_} qw{png gif}
};

helper url_escape => sub {
  my $selft = shift;
  my $s = shift;
  return b($s)->xml_escape;
};

require File::Temp;


## plugin 'TagHelpers';

hook after_render => sub {
  my ($self, $output, $format) = @_;
  
  # Check if "gzip => 1" has been set in the stash
  return unless $self->stash->{gzip};
  
  # Check if user agent accepts GZip compression
  return unless ($self->req->headers->accept_encoding // '') =~ /gzip/i;
  
  # Compress content with GZip
  $self->res->headers->content_encoding('gzip');
  gzip $output, my $compressed;
  $$output = $compressed;
};

get '/process' => sub {
  my $self = shift;
  $ip = $self->ip;
  my $xoptions = b($self->param('options'))->squish;
  my $words = b($self->param('words'))->squish;
  my @words = split(/\s+/,$words);
  my $data = join("\n",
		  map {"$_->[0]\td=$_->[1]"}
		  @{find_closest_words(@words)}
		 );
  $self->stash(words => $words);
  $self->stash(depglove_data => $data);
  $self->res->headers->header('Access-Control-Allow-Origin' => '*'); 
  ##    print $data;
  $self->respond_to(
		    'txt' => {text => $data},
		    'any' => sub { $self->render('process') }
		   );
};

get '/search' => sub {
  my $self = shift;
  my $prefix = b($self->param('w'))->squish;
  print "search prefix=$prefix\n";
  my $ref = [grep {index($_,$prefix) == 0} keys %$vectors ];
  $self->render( json => $ref );
};

any '/*whatever' => {whatever => ''} => sub {
  my $c        = shift;
  my $whatever = $c->param('whatever');
  $c->render(text => "/$whatever is not an allowed request (please try /process) !", status => 404);
};

######################################################################
## we start the Mojolicious application

app->start;

__DATA__

@@ process.html.ep
% layout 'mylayout';

<p>
  Welcome to <b>DEPGLOVE Small Server</b>
</p>

  <p>
  A basic service to try the embedding vectors built from dependency paths using DepGlove, a home-made variant of <a href="http://nlp.stanford.edu/projects/glove/">Glove</a> extended to handle syntactic paths.
  <ul>
  <li>The vectors are associated to pairs (lemma,part of speech), expressed as <b>lemma_pos</b> (eg. <b>homme_nc</b> or <b>indiquer_v</b>).</li>
  <li>The main part-of-speech are <b>nc</b> (common nouns), <b>np</b> (proper nouns), <b>v</b> (verb), <b>adj</b> (adjectives), <b>adv</b> (adverbs)</li>
  <li>The vectors are of dimension 200, and have been extracted from an old dump of the French wikipedia, parsed with <a href="http://alpage.inria.fr/frmgwiki">FRMG</a></li>
  <li>When providing one or more lemma (separated by spaces), the server will return the 10 most similar lemmas</li>
  <li>A lemma may be prefixed by a minus sign to try analogies, for instance <code>-homme_nc femme_nc fils_nc</code> to get fille_nc</li>
  </ul>
  
  </p>
  
%= form_for 'process' => begin
<label for="words">Enter your (French) words</label> </br>
  %= text_area 'words', cols => 70, id => 'words'
<br/>
  %= submit_button
% end

<div id="depglove_output">
  <pre>
%= $depglove_data
  </pre>
</div>
%

@@ exception.production.html.ep
% layout 'mylayout';

<p>
Sorry, but your request failed. If you believe this is a bug, please contact the maintainer of this service
</p>

@@ not_found.production.html.ep
% layout 'mylayout';

<p>
Not found
</p>

@@ layouts/mylayout.html.ep
<!DOCTYPE html>
<html>
    <head>
       <title><%= config 'title' %></title>
%= stylesheet begin
    BODY {
       background-color: linen;
       font-family: verdana, sans-serif;
       font-size: 13px;
    }
    img { border-width: 0px; }
% end
%= asset "app.js"
%= asset "app.css"
%= javascript begin
$(function() {
$('#words').autocomplete({
			source: function (request,response) {
			    var t = request.term;
                            var tokens = t.split(" ");
			    $.getJSON(
				      '/search',
				      {w: tokens.pop()},
				      function(data) { response($.map(data,function (item){ return tokens.join(" ") + " " + item;})) }
				     );
			  },
			minLength: 3,
			delay: 100  
});
});  
% end  
    </head>
    <body><%= content %>
    <hr/>
    <br/>
  <a href="mailto:<%= config 'email'%>"><%= config 'author' %></a>
  <a href="<%= config 'url' %>">Home Page</a>
</body>
</html>

@@ svg.html.ep
%= javascript "js/d3.v3.min.js"
<div  id="depglove_output">
%== $svg
</div>
%= javascript begin
var div = d3.select("#depglove_output");
var svg = d3.select("#depglove_svg").attr("width","100%");
var g = svg.select("g");
var transform = g.attr("transform");
// zoom and pan
var zoom = d3.behavior.zoom()
    .on("zoom",function() {
      var mouse = d3.mouse(this);
        g.attr("transform","scale("+d3.event.scale+")" + "translate(" + d3.event.translate[0] + "," + Math.max(-50,d3.event.translate[1]) + ") " + transform);       
});
svg.call(zoom);
% end


